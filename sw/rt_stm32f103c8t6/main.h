/**
 * @file main.h
 * @brief Control de alarma para motocicleta.
 * @author Johnny Cubides
 * contact: jgcubidesc@gmail.com
 * @date: Mar 28, 2018
 * */

#ifndef MAIN_H
#define MAIN_H

/**Control*/
#define CONTROL_D0 0 //A
#define CONTROL_D1 1 //A
#define CONTROL_D2 2 //A
#define CONTROL_D3 3 //A
#define CONTROL_VT 4 //A
//#define CONTROL_VT 14 //A
/**Relé*/
#define RELE 5 //A
/**GPS*/
#define TX 6 //B
#define RX 7 //B
/**Giroscopio*/
#define SCL 8 //B
#define SDA 9 //B
/**Sensores de choque*/
#define VERTICAL 12 //B
#define HORIZONTAL 13 //B

#define B GPIOB
#define A GPIOA
#define C GPIOC

#define LED 13

/** Giroscop register */
#define ADDRS_START_GIROSCOPE      0x43
#define GYRO_XOUT_H      0x43
#define GYRO_XOUT_L      0x44
#define GYRO_YOUT_H      0x45
#define GYRO_YOUT_L      0x46
#define GYRO_ZOUT_H      0x47
#define GYRO_ZOUT_L      0x48

// UARTS PINS
/** UART RX */
#define TX_UART 10
/** UART RX */
#define RX_UART 11



/*! \brief Brief configuracion de acciones para interrupciones
 *
 *  Detailed description of the function
 *
 * \return void parameter description
 */
static void extcb_sensors(EXTDriver *extp, expchannel_t channel);

/** Variables */

bool enable_alarm = true; /*!< habilitador de alarma para moto*/

uint8_t counter_touch = 0;

/*! \enum state
 *
 *  state
 */
enum State { ACKNOWLEDGE, REQUEST };
/*! \enum request
 *
 *  request
 */
enum Request { ENABLE_ALARM, DISABLE_ALARM, TOGGLE_SIREN, START_MOTOR, WARNING_SIREN, START_ALARM, READ_GIROSCOPE };

struct Event_request {
    enum State state;
    enum Request request;
};

struct Event_request event_request;

struct Giroscope {
    uint16_t x; /*!< eje x */
    uint16_t y; /*!< eje y */
    uint16_t z; /*!< eje z */
    uint16_t x_ref; /*!< eje x */
    uint16_t y_ref; /*!< eje y */
    uint16_t z_ref; /*!< eje z */
};

struct Giroscope giroscope;

/** I2C config */
static const I2CConfig i2cfg1 = {
    OPMODE_I2C,
    400000,
    FAST_DUTY_CYCLE_2,
};

static uint8_t rx_i2c[6];
systime_t tmo = MS2ST(4);

/*! \brief received 2bytes from i2c
 *
 *  i2c uint16_t rx
 *
 * \param *i2cpointer driver
 * \param i2c_addr adress to read
 * \return uint16_t
 */
void i2c_receive_giroscope(I2CDriver *i2cpointer, i2caddr_t i2c_addr, bool giroscope_ref);

/*! \brief callback function for interrupt external from remote control
 *
 *  Funcion de retorno para callback
 *
 * \return void
 */
static void extcb_remote_control(EXTDriver *externalpointer, expchannel_t extchannel);

/*! \brief callback function giroscope
 *
 *  giroscope i2c
 *
 * \return void
 */
static void extcb_i2c_giroscope(EXTDriver *externalpointer, expchannel_t extchannel);

/** habilitacion para uso de interrupciones por hardware*/
static const EXTConfig extcfg = {
	{
		{EXT_CH_MODE_DISABLED, NULL},/*!< 0*/
		{EXT_CH_MODE_DISABLED, NULL},//1
		{EXT_CH_MODE_DISABLED, NULL},//2
		{EXT_CH_MODE_DISABLED, NULL},//3
		{EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOA, extcb_remote_control},//4
		{EXT_CH_MODE_DISABLED, NULL},//5
		{EXT_CH_MODE_DISABLED, NULL},//6
		{EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, extcb_i2c_giroscope},//7
		{EXT_CH_MODE_DISABLED, NULL},//8
		{EXT_CH_MODE_DISABLED, NULL},//9
		{EXT_CH_MODE_DISABLED, NULL},//10
		{EXT_CH_MODE_DISABLED, NULL},//11
		{EXT_CH_MODE_BOTH_EDGES | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, extcb_sensors},//12
		{EXT_CH_MODE_BOTH_EDGES | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, extcb_sensors},//13
		{EXT_CH_MODE_DISABLED, NULL},//14
		{EXT_CH_MODE_DISABLED, NULL}//15
	},
};

static const SerialConfig sd1cfg = {
    115200,
    0,
    0,
    0
};

#endif /* ifndef MAIN_H */

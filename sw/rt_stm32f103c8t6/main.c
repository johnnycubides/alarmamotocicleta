/**
 * @file main.c
 * @brief Control de alarma para motocicleta.
 * @author Johnny Cubides
 * contact: jgcubidesc@gmail.com
 * @date: Mar 28, 2018
 * */
/*
    ChibiOS - Copyright (C) 2006..2016 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <stdio.h>
#include <string.h>

#include "ch.h"
#include "hal.h"
#include "ch_test.h"

//#include "shell.h"
#include "chprintf.h"

//#include "usbcfg.h"

#include "main.h"

/*===========================================================================*/
/* Command line related.                                                     */
/*===========================================================================*/
// Pinout defines

/** @brief Función que será el retorno de las interrupciones
 * @return void */
static void extcb_sensors(EXTDriver *externalpointer, expchannel_t extchannel) {
	(void)externalpointer;
	(void)extchannel;
    if (enable_alarm == true && event_request.state != REQUEST) {
        if(counter_touch < 1)
        {
            event_request.request = WARNING_SIREN;
            event_request.state = REQUEST;
        }else
        {
            event_request.request = START_ALARM;
            event_request.state = REQUEST;
        }
    }
}


void i2c_receive_giroscope(I2CDriver *i2cpointer, i2caddr_t i2c_addr, bool giroscope_ref)
{
    msg_t status_ = MSG_OK;
    status_ = i2cMasterReceiveTimeout(i2cpointer, i2c_addr, rx_i2c, 6, tmo);
    if(status_ == MSG_RESET){

    }else if(giroscope_ref == false){
        giroscope.x = (rx_i2c[0] << 8) + rx_i2c[1];
        giroscope.y = (rx_i2c[2] << 8) + rx_i2c[3];
        giroscope.x = (rx_i2c[4] << 8) + rx_i2c[5];
    }else if(giroscope_ref == true)
    {
        giroscope.x_ref = (rx_i2c[0] << 8) + rx_i2c[1];
        giroscope.y_ref = (rx_i2c[2] << 8) + rx_i2c[3];
        giroscope.x_ref = (rx_i2c[4] << 8) + rx_i2c[5];
    }
}

static void extcb_remote_control(EXTDriver *externalpointer, expchannel_t extchannel)
{
	(void)externalpointer;
	(void)extchannel;
    if (palReadPad(A, CONTROL_D2) == PAL_HIGH) /** Si activa el boton A, habilita alarma*/
    {
        event_request.request = ENABLE_ALARM;
        event_request.state = REQUEST;
    }else if(palReadPad(A, CONTROL_D0) == PAL_HIGH) /** Si activa el boton B, deshabilita alarma*/
    {
        enable_alarm = false;
        palClearPad(A, RELE);
        event_request.request = DISABLE_ALARM;
        event_request.state = REQUEST;
    }else if(palReadPad(A, CONTROL_D3) == PAL_HIGH) /** Si activa el boton C, activa o desactiva sirena*/
    {
        event_request.request = TOGGLE_SIREN;
        event_request.state = REQUEST;
    }else if (palReadPad(A, CONTROL_D1) == PAL_HIGH) /*  */
    {}
}

static void extcb_i2c_giroscope(EXTDriver *externalpointer, expchannel_t extchannel)
{
	(void)externalpointer;
	(void)extchannel;
    event_request.request = READ_GIROSCOPE;
    event_request.state = REQUEST;
}
/*
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg)
{
    (void)arg;
    chRegSetThreadName("blinker");
    while(true){
        palTogglePad(C, LED);
        chThdSleepMilliseconds(1000);
    }
}
*/
static THD_WORKING_AREA(waThread2, 128);
static THD_FUNCTION(Thread2, arg)
{
    (void)arg;
    chRegSetThreadName("atencion_a_perifericos");
    while(true){
        if( event_request.state == REQUEST)
        {
            if(event_request.request == READ_GIROSCOPE)
            {
                i2c_receive_giroscope(&I2CD1, ADDRS_START_GIROSCOPE, false);
                event_request.state = ACKNOWLEDGE;
            }else if(event_request.request == WARNING_SIREN)
            {
                counter_touch++;
                palSetPad(A, RELE);
                chThdSleepMilliseconds(1000);
                palClearPad(A, RELE);
                chThdSleepMilliseconds(500);
                event_request.state = ACKNOWLEDGE;
            }else if (event_request.request == START_ALARM)
            {
                palSetPad(A, RELE);
                chThdSleepMilliseconds(30000);
                palClearPad(A, RELE);
                event_request.state = ACKNOWLEDGE;
            }else if(event_request.request == ENABLE_ALARM)
            {
                palSetPad(A, RELE); // Actived
                chThdSleepMilliseconds(500);
                palClearPad(A, RELE); //non actived
                enable_alarm = true;
                //newi2c referenced giroscope
                //i2c_receive_giroscope(&I2CD1, ADDRS_START_GIROSCOPE, true);
                event_request.state = ACKNOWLEDGE;
            }else if(event_request.request == DISABLE_ALARM)
            {
                palSetPad(A, RELE); // Actived
                chThdSleepMilliseconds(500);
                palClearPad(A, RELE); //non actived
                chThdSleepMilliseconds(500);
                palSetPad(A, RELE); // Actived
                chThdSleepMilliseconds(500);
                palClearPad(A, RELE); //non actived
                event_request.state = ACKNOWLEDGE;
            }else if(event_request.request == TOGGLE_SIREN)
            {
                palTogglePad(A, RELE);
                event_request.state = ACKNOWLEDGE;
            }
        }
        chThdSleepMilliseconds(250);
    }
}

static THD_WORKING_AREA(waThread3, 128);
static THD_FUNCTION(Thread3, arg)
{
    (void)arg;
    chRegSetThreadName("stop counter?");
    while(true){
        chThdSleepMilliseconds(6000);
        counter_touch = 0;
    }
}



/*
 * Application entry point.
 */
int main(void) {
  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  // ################### BEGIN GPIO MODE ############
  palSetPadMode(A, RELE, PAL_MODE_OUTPUT_PUSHPULL); //RELE
  palSetPadMode(B, HORIZONTAL, PAL_MODE_INPUT_PULLDOWN); //HORIZONTAL
  palSetPadMode(B, VERTICAL, PAL_MODE_INPUT_PULLDOWN); //VERTICAL
  palSetPadMode(B, CONTROL_D0, PAL_MODE_INPUT_PULLDOWN); //CONTROL_D0
  palSetPadMode(B, CONTROL_D1, PAL_MODE_INPUT_PULLDOWN); //CONTROL_D1
  palSetPadMode(B, CONTROL_D2, PAL_MODE_INPUT_PULLDOWN); //CONTROL_D2
  palSetPadMode(B, CONTROL_D3, PAL_MODE_INPUT_PULLDOWN); //CONTROL_D3
  palSetPadMode(A, CONTROL_VT, PAL_MODE_INPUT_PULLDOWN); //CONTROL_VT
  palSetPadMode(C, LED, PAL_MODE_OUTPUT_PUSHPULL); //LED
  palClearPad(A, RELE);

  // ################### END GPIO MODE ############



  // ################### BEGIN MODULES ############
  extInit(); // Initialise le driver EXTI
  extStart(&EXTD1, &extcfg);
  //extChannelEnable(&EXTD1, VERTICAL);
  extChannelEnable(&EXTD1, 0);
  i2cStart(&I2CD1, &i2cfg1);

  sdStart(&SD3, &sd1cfg);
  //UART
  //palSetPadMode(B, TX_UART, PAL_MODE_OUTPUT_PUSHPULL);
  //palSetPadMode(B, RX_UART, PAL_MODE_INPUT_PULLDOWN);
  //UART
  //palSetPadMode(B, TX_UART, PAL_MODE_ALTERNATE(7));
  //palSetPadMode(B, RX_UART, PAL_MODE_ALTERNATE(7));
  // ################### END MODULES ############

  // When the reset or first init then giroscope referenced.
  //i2c_receive_giroscope(&I2CD1, ADDRS_START_GIROSCOPE, true);

  //########### BEGIN Inicializacion de multihilos ##############
  /*
   * Creates the blinker thread.
   */
  //chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO, Thread2, NULL);
  chThdCreateStatic(waThread3, sizeof(waThread3), NORMALPRIO, Thread3, NULL);
  //############ END #####################
  /*
   * Normal main() thread activity, spawning shells.
   */
  while (true) {
    /*if (SDU1.config->usbp->state == USB_ACTIVE) {
      thread_t *shelltp = chThdCreateFromHeap(NULL, SHELL_WA_SIZE,
                                              "shell", NORMALPRIO + 1,
                                              shellThread, (void *)&shell_cfg1);
      chThdWait(shelltp);               // Waiting termination.
    }*/
		/*
    if ( palReadPad(A, HORIZONTAL) == PAL_LOW ){
		palSetPad(A, RELE); //Sets a pad logical state to PAL_HIGH.
    } else if ( palReadPad(A, HORIZONTAL) == PAL_HIGH ){
		palClearPad(A, RELE); //Clears a pad logical state to PAL_LOW.
		}*/
    //chprintf((BaseSequentialStream*)&SD3,"Prueba USART\n\r");
    //sdWrite(&SD3, (uint8_t *)"Button Pressed!\r\n", 17);
    chThdSleepMilliseconds(1000);
  }
}

#Primeros pasos con raspberry#

Debe descargar un sistema operativo para la raspberry, en éste caso
instalar **Raspbian**

[raspbian](https://downloads.raspberrypi.org/raspbian_lite_latest)

## Instalación de Raspbian ##

[fuente instalación](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md)

* df -h: Muestra los dispositivos conectados al PC.

Comando que permite la instalación de raspbian en el raspberry pi.
* sudo dd bs=4M if=2017-11-29-raspbian-stretch-lite.img of=/dev/mmcblk0 status=progress conv=fsync

#Levantar conexión ssh#

En la raspberry a través de comunicación serial hacer el siguiente comando:

* sudo raspi-config

En el menú en:
* 5 Interfacing Options            Configure connections to peripher

Habilitar la opción

* P2 SSH                           Enable/Disable remote command linin
 
# Medio Ethernet #

Basta con conectar un cable desde el PC al raspberry pi para tener acceso a la raspberry pi.

# Medio wifi como Station #

Al hace uso del comando **sudo raspi-config** se puede buscar la opción **2 Network Options                Configure network settings** y allí escoger la opción **N2 Wi-fi                         Enter SSID and passphrase**, desde allí agregar el nombre de red y la clave para que la raspberry se conecte a la red.
